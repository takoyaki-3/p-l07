#include <stdio.h>

void swap(int *ap, int *bp) {
  int t;
  t = *ap;
  *ap = *bp;
  *bp = t;
  return;
}

int main(void) {
  int a, b;
  scanf("%d %d", &a, &b);
  swap(&a, &b);
  printf("%d %d\n", a, b);
  return 0;
}
