#include <stdio.h>

int duplicatek(int n, int k, int a[]){
  int i=0,j;
  for(i=0;i<n;i++){
    int c=0;
    for(j=0;j<n;j++){
      if(a[i]==a[j]) c++;
    }
    if(c >= k) return 1;
  }
  return 0;
}

int main(void) {
  int n,k;
  scanf("%d %d", &n,&k);
  int a[100002];

  int i;
  for(i=0;i<n;i++){
    scanf("%d",&a[i]);
  }
  printf("%d\n",duplicatek(n,k,a));
  return 0;
}
