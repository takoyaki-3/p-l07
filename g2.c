#include <stdio.h>

int uniq(int n, int a[]){
  int i,j;
  for(i=0;i<n;i++){
    for(j=i+1;j<n;j++){
      if(a[i]==a[j]) return 0;
    }
  }
  return 1;
}

int main(void) {
  int n;
  scanf("%d", &n);
  int a[100002];
  int i;
  for(i=0;i<n;i++){
    scanf("%d",&a[i]);
  }
  printf("%d\n", uniq(n,a));
  return 0;
}
