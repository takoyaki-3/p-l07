#include <stdio.h>

void sort3(int *ap, int *bp, int *cp){
  int a[3];
  a[0] = *ap,a[1] = *bp,a[2]=*cp;
  int i,j;
  for(i=0;i<3;i++){
    for(j=i+1;j<3;j++){
      if(a[i]>a[j]){
        int t=a[i];
        a[i]=a[j];
        a[j]=t;
      }
    }
  }
  *ap=a[0];
  *bp=a[1];
  *cp=a[2];
}

int main(void) {
  int a, b, c;
  scanf("%d %d %d", &a, &b, &c);
  sort3(&a, &b, &c);
  printf("%d %d %d\n", a, b, c);
  return 0;
}
