#include <stdio.h>

int nearest_point(int n, double x[], double y[]){
  int i=0,minn=2<<28,ind=-1;
  for(i=0;i<n;i++){
    int r = x[i]*x[i]+y[i]*y[i];
    if(minn<r) continue;
    minn=r;
    ind=i;
  }
  return ind;
}

int main(void) {
  int n;
  scanf("%d", &n);
  double x[100002];
  double y[100002];

  int i;
  for(i=0;i<n;i++){
    scanf("%lf",&x[i]);
  }
  for(i=0;i<n;i++){
    scanf("%lf",&y[i]);
  }
  printf("%d\n", nearest_point(n,x,y));
  return 0;
}
