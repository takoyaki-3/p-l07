#include <stdio.h>

int num_visible_buildings(int n, int h[]){
  int i,mh=0,c=0;
  for(i=0;i<n;i++){
    if(mh >= h[i]) continue;
    c++;
    mh=h[i];
  }
  return c;
}

int main(void) {
  int n;
  scanf("%d", &n);
  int h[100002];

  int i;
  for(i=0;i<n;i++){
    scanf("%d",&h[i]);
  }
  printf("%d\n",num_visible_buildings(n,h));
  return 0;
}
